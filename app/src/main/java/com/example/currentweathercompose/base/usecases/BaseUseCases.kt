package com.example.currentweathercompose.base.usecases

interface BaseUseCaseWithAnyInput<in I, out O> {
    suspend fun process(input: I): O
}