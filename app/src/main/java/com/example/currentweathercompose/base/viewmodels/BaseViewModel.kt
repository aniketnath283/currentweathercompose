package com.example.currentweathercompose.base.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.currentweathercompose.base.usecases.BaseUseCaseWithAnyInput
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

abstract class BaseViewModel: ViewModel() {

    val dispatcher by lazy {
        Dispatchers.IO
    }

    protected inline fun <I, O>fetchBaseUseCaseWithAnyInput(
        baseUseCaseWithAnyInput: BaseUseCaseWithAnyInput<I, O>,
        input: I,
        crossinline response : (out : O) -> Unit
    ) {
        viewModelScope.launch(dispatcher) {
            response(baseUseCaseWithAnyInput.process(input))
        }
    }
}