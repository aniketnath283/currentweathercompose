package com.example.currentweathercompose.base.views

import android.os.Bundle
import android.widget.Toast
import com.example.currentweathercompose.base.viewmodels.BaseViewModel
import kotlin.reflect.KClass

abstract class BaseSessionViewModelComposeActivity<VM: BaseViewModel>(viewModelClass: KClass<VM>)
    : BaseViewModelComposeActivity<VM>(viewModelClass) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getViewmodel().setupInitialApiCalls()
    }

    open fun VM.setupInitialApiCalls() {}

    open fun VM.takeActionOnError() {}

    protected fun showError(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }
}


