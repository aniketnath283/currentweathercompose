package com.example.currentweathercompose.base.views

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.example.currentweathercompose.ui.theme.CurrentWeatherComposeTheme
import com.example.currentweathercompose.ui.theme.white

abstract class BaseComposeActivity: ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CurrentWeatherComposeTheme {
                Surface(modifier = Modifier.fillMaxSize(), color = white) {
                    SetupViews()
                }
            }
        }
    }

    @Composable
    open fun SetupViews() {}
}
