package com.example.currentweathercompose.base.entities

data class AppData<T>(
    var data: T? = null,
    var isLoading: Boolean = false,
    var errorMsg: String? = null
)
