package com.example.currentweathercompose.base.views

import android.os.Bundle
import androidx.compose.runtime.Composable
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.currentweathercompose.base.viewmodels.BaseViewModel
import kotlinx.coroutines.launch
import kotlin.reflect.KClass

abstract class BaseViewModelComposeActivity<VM: BaseViewModel>(
    viewModelClass: KClass<VM>
): BaseComposeActivity() {

    private val viewModel by lazy {
        ViewModelProvider(this, defaultViewModelProviderFactory)[viewModelClass.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.observeViewModel()
            }
        }
    }

    @Composable
    override fun SetupViews() {
        viewModel.SetupViews()
    }

    @Composable
    open fun VM.SetupViews(){}

    open fun VM.observeViewModel() {}

    protected fun getViewmodel() = viewModel
}