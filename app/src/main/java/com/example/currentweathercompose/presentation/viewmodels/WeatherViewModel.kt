package com.example.currentweathercompose.presentation.viewmodels

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.example.currentweathercompose.base.entities.AppData
import com.example.currentweathercompose.base.utils.Resource
import com.example.currentweathercompose.base.viewmodels.BaseViewModel
import com.example.currentweathercompose.domain.entities.WeatherInfo
import com.example.currentweathercompose.domain.usecases.LaunchParams
import com.example.currentweathercompose.domain.usecases.WeatherUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class WeatherViewModel @Inject constructor(private val weatherUseCase: WeatherUseCase) :
    BaseViewModel() {

    var weatherInfoState by mutableStateOf(AppData<WeatherInfo>())

    fun getCurrentWeather(lat: Double, lon: Double) {
        weatherInfoState = weatherInfoState.copy(
            isLoading = true
        )
        fetchBaseUseCaseWithAnyInput(weatherUseCase, LaunchParams(lat = lat, lon = lon)) { result ->
            weatherInfoState = when(result) {
                is Resource.Error -> {
                    weatherInfoState.copy(
                        isLoading = false,
                        errorMsg = result.message
                    )
                }

                is Resource.Success -> {
                    weatherInfoState.copy(
                        isLoading = false,
                        data = result.data
                    )
                }
            }
        }
    }
}