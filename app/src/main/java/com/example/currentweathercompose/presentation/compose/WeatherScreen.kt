package com.example.currentweathercompose.presentation.compose

import android.accessibilityservice.AccessibilityService.SoftKeyboardController
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import com.example.currentweathercompose.base.entities.AppData
import com.example.currentweathercompose.domain.entities.WeatherInfo
import com.example.currentweathercompose.presentation.entities.ViewTitles
import com.example.currentweathercompose.ui.theme.black
import com.example.currentweathercompose.ui.theme.dimen_4dp
import com.example.currentweathercompose.ui.theme.dimen_8dp
import com.example.currentweathercompose.ui.theme.red
import com.example.currentweathercompose.ui.theme.white

@Composable
fun WeatherScreen(
    weatherInfo: AppData<WeatherInfo>,
    viewTitles: ViewTitles,
    getWeatherClicked: (lat: String, lon: String) -> Unit
) {
    var latState by remember { mutableStateOf("") }
    var lonState by remember { mutableStateOf("") }
    var errorStateLat by remember { mutableStateOf(false) }
    var errorStateLon by remember { mutableStateOf(false) }
    val controller = LocalSoftwareKeyboardController.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(white)
            .padding(horizontal = dimen_8dp, vertical = dimen_4dp)
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),
            value = latState,
            onValueChange = { value ->
                if (value.isNotEmpty()) {
                    errorStateLat = false
                }
                latState = value
            },
            label = { Text(text = "Lat") },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
            textStyle = TextStyle(color = black)
        )
        if (errorStateLat) {
            Text(modifier = Modifier.fillMaxWidth(), text = "Lat can't be empty", color = red)
        }
        Spacer(modifier = Modifier.height(dimen_4dp))
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),
            value = lonState,
            onValueChange = { value ->
                if (value.isNotEmpty()) {
                    errorStateLon = false
                }
                lonState = value
            },
            label = { Text(text = "Lon") },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
            textStyle = TextStyle(color = black)
        )
        if (errorStateLon) {
            Text(modifier = Modifier.fillMaxWidth(), text = "Lon can't be empty", color = red)
        }
        Spacer(modifier = Modifier.height(dimen_4dp))
        Button(onClick = {
            if (latState.trim().isEmpty()) {
                errorStateLat = true
            }
            if (lonState.trim().isEmpty()) {
                errorStateLon = true
            }
            if (latState.trim().isNotEmpty() && lonState.trim().isNotEmpty()) {
                controller?.hide()
                getWeatherClicked.invoke(latState, lonState)
            }
        }, colors = ButtonDefaults.buttonColors(containerColor = black)) {
            Text(text = "Get Weather", color = white)
        }
        Spacer(modifier = Modifier.height(dimen_4dp))
        if (weatherInfo.isLoading) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(dimen_8dp),
                horizontalArrangement = Arrangement.Center
            ) {
                CircularProgressIndicator()
            }
        }
        weatherInfo.data?.let { weatherInfo ->
            WeatherStatus(
                modifier = Modifier
                    .fillMaxSize()
                    .background(white)
                    .padding(dimen_4dp),
                weatherInfo = weatherInfo,
                viewTitles = viewTitles
            )
        }

    }
}

@Composable
@Preview
fun PreviewWeatherScreen() {
    WeatherScreen(
        weatherInfo = AppData(
            WeatherInfo(
                temp = 27.5,
                tempMin = 23.4,
                tempMax = 30.1,
                humidity = 55,
                visibility = 1000,
                name = "Barrackpore"
            )
        ), viewTitles = ViewTitles()
    ) { lat, lon ->

    }
}