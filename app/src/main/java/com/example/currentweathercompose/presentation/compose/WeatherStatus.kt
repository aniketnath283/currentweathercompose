package com.example.currentweathercompose.presentation.compose

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.currentweathercompose.domain.entities.WeatherInfo
import com.example.currentweathercompose.presentation.entities.ViewTitles
import com.example.currentweathercompose.ui.theme.black
import com.example.currentweathercompose.ui.theme.dimen_4dp
import com.example.currentweathercompose.ui.theme.dimen_8dp
import com.example.currentweathercompose.ui.theme.white

@Composable
fun WeatherStatus(
    modifier: Modifier,
    weatherInfo: WeatherInfo,
    viewTitles: ViewTitles
) {
    Card(
        modifier = modifier
            .background(white), elevation = CardDefaults.cardElevation(dimen_4dp)
    ) {

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .height(IntrinsicSize.Max)
                .background(white)
                .padding(
                    dimen_8dp
                )
        ) {
            weatherInfo.name?.let { place ->
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight()
                        .weight(1f)
                ) {
                    Text(
                        text = viewTitles.place,
                        color = black,
                        style = MaterialTheme.typography.headlineSmall
                    )
                    Text(
                        text = " $place",
                        color = black,
                        style = MaterialTheme.typography.headlineSmall
                    )
                }
                weatherInfo.temp?.let { temp ->
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentHeight()
                            .weight(1f)
                    ) {
                        Text(
                            text = viewTitles.temperature,
                            color = black
                        )
                        Text(
                            text = " $temp",
                            color = black
                        )
                    }
                }
                weatherInfo.tempMax?.let { tempMax ->
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentHeight()
                            .weight(1f)
                    ) {
                        Text(
                            text = viewTitles.maxTemp,
                            color = black
                        )
                        Text(
                            text = " $tempMax",
                            color = black
                        )
                    }
                }
                weatherInfo.tempMin?.let { tempMin ->
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentHeight()
                            .weight(1f)
                    ) {
                        Text(
                            text = viewTitles.minTemp,
                            color = black
                        )
                        Text(
                            text = " $tempMin",
                            color = black
                        )
                    }
                }
                weatherInfo.humidity?.let { humidity ->
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentHeight()
                            .weight(1f)
                    ) {
                        Text(
                            text = viewTitles.humidity,
                            color = black
                        )
                        Text(
                            text = " $humidity",
                            color = black
                        )
                    }
                }
                weatherInfo.visibility?.let { visibility ->
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentHeight()
                            .weight(1f)
                    ) {
                        Text(
                            text = viewTitles.visibility,
                            color = black
                        )
                        Text(
                            text = " $visibility",
                            color = black
                        )
                    }
                }
            }
        }
    }
}

@Composable
@Preview
fun PreviewWeatherStatus() {
    WeatherStatus(
        weatherInfo = WeatherInfo(
            temp = 27.5,
            tempMin = 23.4,
            tempMax = 30.1,
            humidity = 55,
            visibility = 1000,
            name = "Barrackpore"
        ),
        viewTitles = ViewTitles(),
        modifier = Modifier
            .fillMaxSize()
            .background(white)
    )
}