package com.example.currentweathercompose.presentation.activity

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.currentweathercompose.R
import com.example.currentweathercompose.base.views.BaseSessionViewModelComposeActivity
import com.example.currentweathercompose.presentation.compose.WeatherScreen
import com.example.currentweathercompose.presentation.entities.ViewTitles
import com.example.currentweathercompose.presentation.viewmodels.WeatherViewModel
import com.example.currentweathercompose.ui.theme.CurrentWeatherComposeTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseSessionViewModelComposeActivity<WeatherViewModel>(WeatherViewModel::class) {

    private val titles by lazy {
        ViewTitles(
            temperature = getString(R.string.temperature),
            maxTemp = getString(R.string.max_temp),
            minTemp = getString(R.string.min_temp),
            humidity = getString(R.string.humidity),
            visibility = getString(R.string.visibility),
            place = getString(R.string.place)
        )
    }
    @Composable
    override fun WeatherViewModel.SetupViews() {
        val state = this.weatherInfoState
        state.errorMsg?.let {
            showError(it)
        }
        WeatherScreen(weatherInfo = state, viewTitles = titles) { lat, lon ->
            this.getCurrentWeather(lat.toDouble(), lon.toDouble())
        }
    }
}

