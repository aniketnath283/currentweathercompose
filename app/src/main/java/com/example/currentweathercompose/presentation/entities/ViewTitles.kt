package com.example.currentweathercompose.presentation.entities

data class ViewTitles(
    val temperature: String = "Temperature",
    val maxTemp: String = "Max temp",
    val minTemp: String = "Min temp",
    val humidity: String = "Humidity",
    val visibility: String = "Visibility",
    val place: String = "Place"
)
