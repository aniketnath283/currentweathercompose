package com.example.currentweathercompose.domain.repositories

import com.example.currentweathercompose.base.utils.Resource
import com.example.currentweathercompose.domain.entities.WeatherInfo

interface WeatherRemoteRepository {
    suspend fun getWeather(lat: Double, lon: Double): Resource<WeatherInfo>
}