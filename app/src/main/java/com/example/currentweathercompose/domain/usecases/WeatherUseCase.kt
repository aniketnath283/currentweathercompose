package com.example.currentweathercompose.domain.usecases

import com.example.currentweathercompose.base.usecases.BaseUseCaseWithAnyInput
import com.example.currentweathercompose.base.utils.Resource
import com.example.currentweathercompose.domain.entities.WeatherInfo
import com.example.currentweathercompose.domain.repositories.WeatherRepository
import javax.inject.Inject

class WeatherUseCase @Inject constructor(private val weatherRepository: WeatherRepository) :
    BaseUseCaseWithAnyInput<LaunchParams, Resource<WeatherInfo>> {
    override suspend fun process(input: LaunchParams): Resource<WeatherInfo> {
        return weatherRepository.getWeather(input.lat, input.lon)
    }
}

data class LaunchParams(
    val lat: Double,
    val lon: Double
)