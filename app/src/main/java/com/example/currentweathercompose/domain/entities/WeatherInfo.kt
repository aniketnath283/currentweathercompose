package com.example.currentweathercompose.domain.entities

data class WeatherInfo(
    val temp: Double?,
    val tempMin: Double?,
    val tempMax: Double?,
    val humidity: Int?,
    val visibility: Long?,
    val name: String?
)
