package com.example.currentweathercompose.data.remote.repositoris

import com.example.currentweathercompose.base.utils.Resource
import com.example.currentweathercompose.data.remote.apis.WeatherApi
import com.example.currentweathercompose.data.remote.mappers.toWeatherInfo
import com.example.currentweathercompose.domain.entities.WeatherInfo
import com.example.currentweathercompose.domain.repositories.WeatherRemoteRepository
import java.lang.Exception
import java.util.concurrent.CancellationException
import javax.inject.Inject

class WeatherRemoteRepositoryImpl @Inject constructor(private val weatherApi: WeatherApi): WeatherRemoteRepository {
    override suspend fun getWeather(lat: Double, lon: Double): Resource<WeatherInfo> {
        return try {
            return Resource.Success(weatherApi.getCurrentWeather(lat = lat, lon = lon).toWeatherInfo())
        } catch (e: Exception) {
            if (e is CancellationException) {
                throw e
            } else {
                Resource.Error(e.message)
            }
        }
    }
}