package com.example.currentweathercompose.data.remote.mappers

import com.example.currentweathercompose.data.remote.entities.WeatherDto
import com.example.currentweathercompose.domain.entities.WeatherInfo

fun WeatherDto.toWeatherInfo(): WeatherInfo {
    return WeatherInfo(
        temp = this.main?.temp,
        tempMin = this.main?.tempMin,
        tempMax = this.main?.tempMax,
        humidity = this.main?.humidity,
        visibility = this.visibility,
        name = this.name
    )
}