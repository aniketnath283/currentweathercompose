package com.example.currentweathercompose.data.common.repositories

import com.example.currentweathercompose.base.utils.Resource
import com.example.currentweathercompose.domain.entities.WeatherInfo
import com.example.currentweathercompose.domain.repositories.WeatherRemoteRepository
import com.example.currentweathercompose.domain.repositories.WeatherRepository
import javax.inject.Inject

class WeatherRepositoryImpl @Inject constructor(private val weatherRemoteRepository: WeatherRemoteRepository) : WeatherRepository {
    override suspend fun getWeather(lat: Double, lon: Double): Resource<WeatherInfo> {
        return weatherRemoteRepository.getWeather(lat, lon)
    }
}