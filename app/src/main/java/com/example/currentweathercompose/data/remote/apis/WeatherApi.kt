package com.example.currentweathercompose.data.remote.apis

import com.example.currentweathercompose.data.remote.entities.WeatherDto
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET(ApiConstants.WEATHER_END_POINT)
    suspend fun getCurrentWeather(
        @Query(ApiConstants.KEY_APP_ID) appId: String = ApiConstants.VALUE_APP_ID,
        @Query(ApiConstants.KEY_UNITS) unit: String = ApiConstants.VALUE_UNITS_METRIC,
        @Query(ApiConstants.KEY_LAT) lat: Double,
        @Query(ApiConstants.KEY_LON) lon: Double
    ): WeatherDto
}