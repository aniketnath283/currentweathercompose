package com.example.currentweathercompose.data.remote.apis

object ApiConstants {
    const val WEATHER_END_POINT = "2.5/weather"
    const val KEY_APP_ID = "APPID"
    const val VALUE_APP_ID = "9b8cb8c7f11c077f8c4e217974d9ee40"
    const val KEY_UNITS = "units"
    const val VALUE_UNITS_METRIC = "metric"
    const val KEY_LAT = "lat"
    const val KEY_LON = "lon"
}