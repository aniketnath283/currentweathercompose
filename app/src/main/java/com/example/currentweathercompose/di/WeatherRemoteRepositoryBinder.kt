package com.example.currentweathercompose.di

import com.example.currentweathercompose.data.remote.repositoris.WeatherRemoteRepositoryImpl
import com.example.currentweathercompose.domain.repositories.WeatherRemoteRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class WeatherRemoteRepositoryBinder {

    @Binds
    @Singleton
    abstract fun bindWeatherRemoteRepository(weatherRemoteRepositoryImpl: WeatherRemoteRepositoryImpl): WeatherRemoteRepository
}