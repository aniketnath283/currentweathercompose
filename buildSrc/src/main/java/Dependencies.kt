object Keys {
    const val baseUrl = "BASE_URL"
}

object Versions {

    const val androidCoreVersion = "1.12.0"
    const val appcompatVersion = "1.6.1"
    const val androidMatertialVersion = "1.11.0"
    const val constraintLayoutVersion = "2.1.4"
    const val jUnitVersion = "4.13.2"
    const val extJUnitVersion = "1.1.5"
    const val espressoVersion = "3.5.1"
    const val navigationVersion = "2.7.7"
    const val lifecycleVersion = "2.8.0-alpha02"
    const val coroutineVersion = "1.7.3"
    const val retrofitVersion = "2.9.0"
    const val kotlinVersion = "1.9.22"
    const val hiltVersion = "2.48"
    const val coreArchTestVersion = "2.2.0"
    const val mockitoVersion = "4.6.0"
    const val mockitoKotlinVersion = "2.2.0"
    const val turbineVersion = "1.0.0"
    const val gradleVersion = "7.4.2"
    const val mockWebServerVersion = "4.12.0"
    const val r8Version = "8.2.47"
    const val composeBomVersion = "2024.02.02"
    const val activityComposeVersion = "1.8.2"
    const val composeCompilerVersion = "1.5.8"
    const val coilVersion = "2.2.2"
    const val okHttpInterceptorVersion = "5.0.0-alpha.3"

}


object Plugins {
    const val gradle = "com.android.tools.build:gradle:${Versions.gradleVersion}"
    const val gradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinVersion}"
    const val daggerHilt = "com.google.dagger:hilt-android-gradle-plugin:${Versions.hiltVersion}"
    const val r8 = "com.android.tools:r8:${Versions.r8Version}"
}

object Jetbrains {
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlinVersion}"
    const val coroutinesCore =
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutineVersion}"
}

object Androidx {
    const val appCompat = "androidx.appcompat:appcompat:${Versions.appcompatVersion}"
    const val constraintlayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintLayoutVersion}"
    const val core = "androidx.core:core-ktx:${Versions.androidCoreVersion}"
    const val lifecycleLiveData =
        "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycleVersion}"
    const val lifecycleViewModel =
        "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycleVersion}"
    const val lifecycleKapt = "androidx.lifecycle:lifecycle-compiler:${Versions.lifecycleVersion}"
    const val navigationFragment =
        "androidx.navigation:navigation-fragment-ktx:${Versions.navigationVersion}"
    const val lifecycleRuntimeKtx = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycleVersion}"
}

object Google {
    const val materialDesign =
        "com.google.android.material:material:${Versions.androidMatertialVersion}"
}

object Square {
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofitVersion}"
    const val retrofitConverterGson =
        "com.squareup.retrofit2:converter-gson:${Versions.retrofitVersion}"
    const val retrofitOkhttpInerceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okHttpInterceptorVersion}"
}

object Dagger {
    const val daggerHilt = "com.google.dagger:hilt-android:${Versions.hiltVersion}"
    const val daggerCompiler = "com.google.dagger:hilt-compiler:${Versions.hiltVersion}"
}

object Testing {
    const val mockitoKotlin =
        "com.nhaarman.mockitokotlin2:mockito-kotlin:${Versions.mockitoKotlinVersion}"
    const val mockitoCore = "org.mockito:mockito-core:${Versions.mockitoVersion}"
    const val turbine = "app.cash.turbine:turbine:${Versions.turbineVersion}"
    const val junit = "junit:junit:${Versions.jUnitVersion}"
    const val androidArchCoreTesting =
        "androidx.arch.core:core-testing:${Versions.coreArchTestVersion}"
    const val junitTest = "androidx.test.ext:junit:${Versions.extJUnitVersion}"
    const val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espressoVersion}"
    const val coroutineTest =
        "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutineVersion}"
    const val mockServerTest = "com.squareup.okhttp3:mockwebserver:${Versions.mockWebServerVersion}"
    const val composeJunit4 = "androidx.compose.ui:ui-test-junit4"
}

object DebugImplementation {
    const val composeUiTooling = "androidx.compose.ui:ui-tooling"
    const val composeUiTestManifest = "androidx.compose.ui:ui-test-manifest"
}

object Compose {
    const val composeBom = "androidx.compose:compose-bom:${Versions.composeBomVersion}"
    const val activityCompose = "androidx.activity:activity-compose:${Versions.activityComposeVersion}"
    const val composeUi = "androidx.compose.ui:ui"
    const val composeUiGraphics = "androidx.compose.ui:ui-graphics"
    const val composeUiTooling = "androidx.compose.ui:ui-tooling-preview"
    const val composeMaterial3 = "androidx.compose.material3:material3"
    const val coilCompose = "io.coil-kt:coil-compose:${Versions.coilVersion}"
}

